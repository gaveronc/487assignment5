This project compiles correctly under g++ version 4.8.4 using the command:
g++ A5.cpp -o A5

The executable can be run with by invoking ./A5

The program populates a sin lookup table, then runs a test comparing the lookup
table value to the actual sin value, ensuring that the lookup table is accurate
up to at least +/- 0.5 degrees. If the program runs without error, the output
should provide statements saying it is populating the lookup table, testing the
lookup table, and, finally, exiting. Any errors will produce error messages
before exiting. Since this program runs correctly with no errors, these do not
show up when executed.