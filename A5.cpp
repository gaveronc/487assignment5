//////////////////////////////////////////
//Cameron Gaveronski
//ENEL 487
//Assignment 5
//////////////////////////////////////////

//////////////////////////////////////////
//This program creates a sin lookup table,
//then compares the actual values for sin
//to the values in the lookup table.
//////////////////////////////////////////

#include <iostream>//Provide user output
#include <cmath>//Provide sin function

//360 * # of revolutions (one positive, one negative) * # of values per degree (2) - end points
#define TABLE_SIZE ((2 * 2 * 360) - 2)
#define PI 3.14159265//Used to convert between degrees and radians

using namespace std;

int global_sin_table[TABLE_SIZE];//Lookup table for sin
//0 index = sin(-359.5)

void PopulateSin();//Initialize the global sin table
int SinLookup(int angle);//Get sin of angle divided by 10
void TestTable();//Perform tests; no output means test succeeded


int main() {
	//A very simple function: populate the table,
	//then test its accuracy
	cout << "Populating sin lookup table\n"; 
	PopulateSin();
	cout << "Testing table\n";
	TestTable();
	cout << "Testing complete\nExiting\n";
	
	return 0;
}

void PopulateSin() {
	for(int i = 0; i < TABLE_SIZE; i ++) {//Store actual value into array index
		global_sin_table[i] = sin((-359.5 + (0.5*i))*PI/180) * 100000;
	}
}

int SinLookup(int angle) {
	int index = (angle+5)/5 + 718;
	if(index < 0 || index > TABLE_SIZE) {
		cout << "Out of bounds!\n";//Notify of out-of-bounds access
		return 0;
	}
	//Return actual value (rounded and multiplied) from calculated index
	return global_sin_table[unsigned(index)];
}

void TestTable() {
	//Compare actual value to nearest lookup table value
	//and report any variation too large. No output
	//means the lookup table is an acceptable
	//approximation of sin.
	int lookupVal, actual;
	for(int i = -3595; i < 3595; i++) {
		lookupVal = SinLookup(i);//Get lookup value
		actual = sin(i*PI/1800) * 100000;//Get actual value
		if(abs(lookupVal - actual) > abs(sin(i - 5) - sin(i + 5) * 100000)) {
			//If the lookup is too far off from the actual value, notify the user
			cout << "Sin(" << i << ") doesn't look right\n";
			cout << "Actual: " << actual << endl;
			cout << "Lookup: " << lookupVal << endl;
		}
	}
}
